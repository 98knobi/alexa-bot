# Alexa Robot

## About this Project

This Project is for the Firmware of the Robot described in the [Mainproject](https://gitlab.com/98knobi/alexa-bot-server).

It is supposed to run on an NodeMCU V3 wich is a Development-Board for an ESP8266 Microcontroller.

## How to flash this Project

This Project was written using the PlatformIO IDE for VisualStudio Code.

You have to Install the needed Library's from the `platformio.ini` file with the PlatformIO Library Manager.

After installing the Dependency's you can connect the NodeMCU board.

After changing the `upload_port` in the `platformio.ini` you should be able to Upload the Project to the Microcontroller.