#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WebServer.h>

#include <WebSocketsClient.h>

#include <WiFiManager.h>

#include <ArduinoJson.h>

#include <AccelStepper.h>

#define PINENA D1
#define PINDIRA D2
#define PINSTEPA D3

#define PINENB D4
#define PINDIRB D5
#define PINSTEPB D6

ESP8266WiFiMulti WiFiMulti;
WebSocketsClient webSocket;

const int capacity = JSON_OBJECT_SIZE(2);

StaticJsonDocument<capacity> doc;

AccelStepper stepperA(1, PINSTEPA, PINDIRA);
AccelStepper stepperB(1, PINSTEPB, PINDIRB);

void driveForward(unsigned long steps) {
    stepperA.move(steps);
    stepperB.move(steps);
}

void driveBackwards(unsigned long steps) {
    stepperA.move(-steps);
    stepperB.move(-steps);
}

void rotateClockwise(unsigned long steps) {
    stepperA.move(steps);
    stepperB.move(-steps);
}

void rotateCounterClockwise(unsigned long steps) {
    stepperA.move(-steps);
    stepperB.move(steps);
}

void handleMessage(char* messageText) {
    Serial.printf("received message: \"%s\"\n", messageText);
    Serial.println("starting to deserialize the JSON");
    DeserializationError err = deserializeJson(doc, messageText);

    const char* forward = "driveForward";
    const char* backwards = "driveBackwards";
    const char* clockwise = "rotateClockwise";
    const char* counterClockwise = "rotateCounterClockwise";

    if (err) {
        Serial.print(F("deserializeJson() failed with code "));
        Serial.println(err.c_str());
    }
    else {
        const char* command = doc["command"];
        const unsigned long steps = doc["steps"];

        if (strcmp(command, forward) == 0) {
            Serial.printf("driving Forward for %lu steps\n", steps);
            driveForward(steps);
        }
        else if (strcmp(command, backwards) == 0) {
            Serial.printf("driving Backwards for %lu steps\n", steps);
            driveBackwards(steps);
        }
        else if (strcmp(command, clockwise) == 0) {
            Serial.printf("rotating Clockwise for %lu steps\n", steps);
            rotateClockwise(steps);
        }
        else if (strcmp(command, counterClockwise) == 0) {
            Serial.printf("rotating counter Clockwise for %lu steps\n", steps);
            rotateCounterClockwise(steps);
        }
        else {
            Serial.printf("Failed to match an command: '%s'\n", command);
        }
    }
}

void webSocketEvent(WStype_t type, uint8_t * payload, size_t length) {
    switch(type) {
        case WStype_CONNECTED:
            Serial.printf("[WSc] Connected to url: %s\n", payload);
            // send message to server when Connected
            webSocket.sendTXT("Connected");
            break;
        case WStype_ERROR:
            Serial.printf("[WSc] Error: %s\n", payload);
            break;
        case WStype_TEXT:
            handleMessage((char*) payload);
            break;
        default:
            Serial.printf("[WSc] Eventid: %i\n", type);
    }
}

void setup() {
    Serial.begin(9600);

    WiFiManager wifiManager;

    wifiManager.autoConnect("AutoConnectAP");

    Serial.println("Connected to Wifi");

    webSocket.begin("185.101.93.150", 1337, "/");
    webSocket.onEvent(webSocketEvent);
    webSocket.setReconnectInterval(5000);
    webSocket.enableHeartbeat(15000, 3000, 2);

    pinMode(PINENA, OUTPUT);
    pinMode(PINENB, OUTPUT);

    pinMode(PINENA, HIGH);
    pinMode(PINENB, HIGH);

    stepperA.setMaxSpeed(1000);
    stepperA.setAcceleration(1000);

    stepperB.setMaxSpeed(1000);
    stepperB.setAcceleration(1000);
}

void loop() {
    webSocket.loop();

    stepperA.run();
    stepperB.run();
}